{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Systems & Synthetic Biology\n",
    "\n",
    "## COBRApy for FBA\n",
    "\n",
    "In lectures, you were shown how to solve an FBA optimisation problem within Excel spreadsheets. However, this requires a lot of manual work. Computer programs, e.g. COBRApy, have been developed that makes solving FBA problems easier for larger systems. It is these that we will explore today.\n",
    "\n",
    "Today, we will look at creating the toy model looked at in lectures so that you can see how COBRApy works. Then, tomorrow, you will apply this for a pre-made larger model of E. coli metabolism.\n",
    "\n",
    "### Downloading COBRApy\n",
    "\n",
    "If you have not done so already, please download COBRApy following the instructions here: https://opencobra.github.io/cobrapy/\n",
    "\n",
    "In a terminal window, use the command\n",
    "\n",
    "pip install cobra (Mac/Linux)\n",
    "\n",
    "conda install -c conda-forge cobra (Windows)\n",
    "\n",
    "After installation, you should now be able to use COBRApy within a Python environment of your Jupyter notebook.\n",
    "\n",
    "### Creating a model in COBRApy\n",
    "\n",
    "In this exercise we are going to re-create the toy model provided in the Excel spreadsheet using COBRApy syntax. The Excel spreadsheet contains the names of metabolites (denoted by *c* if they are in the cell or *e* if they are extracellular), the reaction names (denoted *R* if they take place in the cell, *T* if they transport metabolites from the cell to the extracellular space, or *Ex* if the metabolite diffuses into/away from the extracellular space), the stoichiometry of metabolites in each reaction, and the bounds within which the flux values are constrained.\n",
    "\n",
    "Before we start building the model, we need to import some functions from Cobra, we will then create an empty model structure (called *toy_model*) then fill our empty model with components and reactions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cobra import Model, Reaction, Metabolite\n",
    "model = Model('toy_model')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The **Model** function creates a **model** structure that includes some default settings (as you will see later) to help analyse a given system. But before we analyse our model, we need to create it. For this we will use the **Metabolite** function to provides labels and compartment information for metabolites in our system, and we will use the **Reaction** function to add connections between metabolites.\n",
    "\n",
    "We will start be creating the metabolites in our system. Let's take a look at two examples to show you how to do this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create metabolite with name and compartment\n",
    "A_c = Metabolite(\n",
    "    'A_c',\n",
    "    name = 'A_c',\n",
    "    compartment = 'c')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This command creates an object referred to as *A_c*, containing details of metabolite *A* that is found in cell compartment *c*.\n",
    "\n",
    "We can do the same for metabolite *A* when it is found in the extracellular medium *e*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A_e = Metabolite(\n",
    "    'A_e',\n",
    "    name = 'A_e',\n",
    "    compartment = 'e')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the code above, create the rest of the metabolites in our system (note these have not yet been added to the model!). \n",
    "\n",
    "These are: *B_c, C_c, D_c, E_c, F_c, G_c, B_e, E_e, F_e, G_e*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can add information about the reactions between these metabolites.\n",
    "\n",
    "Below you will find an example where information for reaction **R1** is created (but not yet added to the model!). In this example, we are adding a reaction name, the bounds between which the flux is constrained, and the stoichiometry of the metabolites involved in the reaction.\n",
    "\n",
    "Note that the stoichiometry here implies that *A_c* is converted to *B_c* (*A_c* $\\rightarrow$ *B_c*) such that 1 molecule of *A_c* is consumed (-1) and 1 molecule of *B_c* is produced (+1).\n",
    "\n",
    "The bounds of the flux enforce that the reaction proceeds in the forward direction only. If the lower bound is some negative value and the upper bound is 0, then the reaction is forced to proceed in the reverse direction only. Whilst, a negative lower bound and positive upper bound suggests that the reaction is reversible and can occur in either direction depending on experimental conditions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create reaction R1\n",
    "reaction_R1 = Reaction('R1')\n",
    "\n",
    "# Give it a name\n",
    "reaction_R1.name = 'R1 ' \n",
    "\n",
    "# Specify the bounds of the flux.\n",
    "# If the lower bound is 0 and the upper bound positive, then the reaction proceeds in a positive direction only\n",
    "# If the lower bound is negative and the upper bound is 0, then the flux only flows in the reverse direction\n",
    "# If the lower bound is negative and the upper bound is positive, then the reaction flux could flow in either direction (to be determined by FBA)\n",
    "reaction_R1.lower_bound = 0 \n",
    "reaction_R1.upper_bound = 100\n",
    "\n",
    "# Add the reaction stoichiometry\n",
    "reaction_R1.add_metabolites({\n",
    "    A_c: -1.0,\n",
    "    B_c: 1.0\n",
    "})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at a second example, this time for reaction **T1**; the exchange reaction whereby metabolite *A* is taken up or secreted by the cell from the extracellular space. Again, the flux bounds force the reaction to occur in one direction, with *A* being taken up from *A_e* to *A_c*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reaction_T1 = Reaction('T1')\n",
    "reaction_T1.name = 'T1 '\n",
    "reaction_T1.lower_bound = 0\n",
    "reaction_T1.upper_bound = 1000\n",
    "reaction_T1.add_metabolites({\n",
    "    A_c: 1.0,\n",
    "    A_e: -1.0\n",
    "})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let's look at an exchange reaction. Here reaction *Ex_a* describes how extracellular *A_e* diffuses towards/away from the cell. \n",
    "\n",
    "Note here that the flux bounds now allow for a negative flux... thus if the reaction is written as \n",
    "\n",
    "*A_e* $\\rightarrow\\varnothing$\n",
    "\n",
    "with some rate *k*, and $\\varnothing$ implies *A_e* is destroyed.\n",
    "\n",
    "In this scenario the flux is written as *v = kA_e*. Metabolite concentration *A_e* can never be negative, so if the flux *v* took a negative value, this means that the reaction rate/direction needs to be reversed from a degradation process to a creation process.\n",
    "\n",
    "Positive flux *v*: *A_e* $\\rightarrow\\varnothing$\n",
    "\n",
    "Negative flux *v*: $\\varnothing\\rightarrow$ *A_e*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reaction_ExA = Reaction('Ex_a')\n",
    "reaction_ExA.name = 'Ex_a '\n",
    "reaction_ExA.lower_bound = -1000\n",
    "reaction_ExA.upper_bound = 1000\n",
    "reaction_ExA.add_metabolites({\n",
    "    A_e: -1.0,\n",
    "})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Following the same notation above, create the rest of the reactions in our toy model:\n",
    "\n",
    "*R2, R3, R4, R5, R6, R7, T2, T3, T4, T5, Ex_b, Ex_e, Ex_f, Ex_g*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have all the reactions and metabolites assigned, we need to add the reactions to our toy model.\n",
    "\n",
    "To do this we need to use the **add_reaction** function that takes a list of reactions as an input."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reaction_list = [reaction_R1,\n",
    "                 reaction_R2,\n",
    "                 reaction_R3,\n",
    "                 reaction_R4,\n",
    "                 reaction_R5,\n",
    "                 reaction_R6,\n",
    "                 reaction_R7,\n",
    "                 reaction_T1,\n",
    "                 reaction_T2,\n",
    "                 reaction_T3,\n",
    "                 reaction_T4,\n",
    "                 reaction_T5,\n",
    "                 reaction_ExA,\n",
    "                 reaction_ExB,\n",
    "                 reaction_ExE,\n",
    "                 reaction_ExF,\n",
    "                 reaction_ExG]\n",
    "\n",
    "model.add_reactions(reaction_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To check that our model is complete, we can run the following print statements to see what is contained within the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The objects have been added to the model\n",
    "print(\"%d reactions\" % len(model.reactions))\n",
    "print(\"%d metabolites\" % len(model.metabolites))\n",
    "\n",
    "# These print statements replace the term %d with the value provided at the end of the command.\n",
    "# You can supply a list of multiple values to be replaced if you wish (see below)\n",
    "# %d refers to a double value (e.g. 1, 25, 100)\n",
    "# %f refers to a float value (e.g 1.0, 25.2, 100.03)\n",
    "# %s refers to a string (e.g. \"one\", \"twenty-five\", \"one hundred\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Iterate through the the objects in the model\n",
    "print(\"Reactions\")\n",
    "print(\"---------\")\n",
    "for x in model.reactions:\n",
    "    print(\"%s : %s\" % (x.id, x.reaction))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now in a position to set an \"objective\" which we wish to optimize. Let's set the objective to maximize *G* secretion into the media, i.e. we want to maximize reaction *Ex_g*.\n",
    "\n",
    "The value contained in model.objective.direction tells us whether the reaction is being maximized or minimized. Note that the model.objective.direction you obtain here is the default option set when we created the model (since we have not changed it!) and should be \"max\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.objective = 'Ex_g'\n",
    "print(model.objective.direction)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have everything we now need --- model structure, flux bounds/constraints, and an objective --- to perform FBA using the optimize() function.\n",
    "\n",
    "We will look at the output by printing the optimal value of *Ex_g* (the objective) and then see which flux-values lead to this optimal value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# obtain solution\n",
    "solution = model.optimize()\n",
    "\n",
    "# print optimal value of our objective function\n",
    "print(\"Optimal objective value Ex_g = %f\" % solution.objective_value)\n",
    "\n",
    "# print a summary of the model fluxes that provide optimal objective value\n",
    "solution.fluxes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You could also use model.summary() to get an overview of the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What we should see from this result is that the cell is only growing due to consumption of *A*. To maximize the secretion of *G*, *A* is taken up and the cell uses this molecule through *R3* to produce *D*. From hereon, the flux of the network flows from *D* to *F* and *G* via *R6*.\n",
    "\n",
    "Let's now consider growth media that our cell grows on in our system. This is an important test to do in real experiments. In many cases we are interested in maximizing growth of cells, or their ability to produce certain compounds - finding optimal growth conditions can go some way to solve this problem before genetic engineering and synthetic biology is required!\n",
    "\n",
    "First, let's check our flux bounds again. COBRApy has a function called medium that outputs the external components of the model and their lower bound for secretion (which would be a positive upper bound for uptake). This gives us the lower bound/negative value of reactions as positive values, i.e. it is these metabolites that *could* be taken up by the cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.medium"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will simulate changes in growth media by changing the bounds of uptake and secretion for our external compounds. First, confirm that the values above are the positive values of the lower bounds for these reactions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Iterate through the the objects in the model\n",
    "print(\"Reaction bounds\")\n",
    "print(\"---------\")\n",
    "for x in model.reactions:\n",
    "    print(\"%s : (%d,%d)\" % (x.id, x.bounds[0],x.bounds[1]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From these bounds we can see that all external components could be present in the growth media and they can be taken up by the cell, i.e. the lower bound is -1000.\n",
    "\n",
    "We are now going to make a pretty big assumption. We will assume that *E*, *F*, and *G* are not present in the growth media. As such there is nothing to be taken up by the cell from the extracellular environment, i.e. the lower bounds for reactions *Ex_e*, *Ex_f*, and *Ex_g* will all be set to zero. \n",
    "\n",
    "This, though, has a secondary effect, namely that any *E*, *F*, or *G* produced by the cell and released into the extracellular environment cannot be taken up again. This may not be biologically realistic, but it allows us to constrain and simulate the system under different growth/environmental conditons.\n",
    "\n",
    "Following the example, set the lower bounds of *Ex_e*, *Ex_f*, and *Ex_g* to 0, and the lower bound of *Ex_a* to -15, i.e. uptake of *A* is constrained compared to other compounds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You could do it this way:\n",
    "# reaction_ExE.lower_bound = 0\n",
    "\n",
    "# Or:\n",
    "model.reactions.Ex_e.lower_bound = 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set lower bounds of Ex_f and Ex_g to 0\n",
    "model.reactions.Ex_f.lower_bound = 0\n",
    "model.reactions.Ex_g.lower_bound = 0\n",
    "\n",
    "# Set lower bound of Ex_a to -15\n",
    "model.reactions.Ex_a.lower_bound = -15\n",
    "\n",
    "# Check that the bounds are correct\n",
    "print(\"Reaction bounds\")\n",
    "print(\"---------\")\n",
    "for x in model.reactions:\n",
    "    print(\"%s : (%d,%d)\" % (x.id, x.bounds[0],x.bounds[1]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's perform FBA and see what the optimal value of *Ex_g* is. How does it compare with the previously found optimal growth rate?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# obtain solution\n",
    "solution = model.optimize()\n",
    "\n",
    "# print optimal value of our objective function\n",
    "print(\"Optimal objective value Ex_g = %f\" % solution.objective_value)\n",
    "\n",
    "# print a summary of the model fluxes that provide optimal objective value\n",
    "solution.fluxes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that, again, the system ignores all the *B* that can be taken up and maximises the *Ex_g* flux by maximising *Ex_a*.\n",
    "\n",
    "So, what happens if we remove *B* from the medium? Set the lower bound of *Ex_b* to 0 and re-optimize the model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set lower bound of Ex_b to 0\n",
    "model.reactions.Ex_b.lower_bound = 0\n",
    "\n",
    "# obtain solution\n",
    "solution = model.optimize()\n",
    "\n",
    "# print optimal value of our objective function\n",
    "print(\"Optimal objective value Ex_g = %f\" % solution.objective_value)\n",
    "\n",
    "# print a summary of the model fluxes that provide optimal objective value\n",
    "solution.fluxes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tells us that *B* is not required to maximize *Ex_g*, but what if *A* is unavailable but *B* is in the media?\n",
    "\n",
    "To test this, set the lower bound of *Ex_b* back to -1000 and set the lower bound of *Ex_a* to 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set lower bound of Ex_a to 0\n",
    "model.reactions.Ex_a.lower_bound = 0\n",
    "\n",
    "# Set lower bound of Ex_b to -1000\n",
    "model.reactions.Ex_b.lower_bound = -1000\n",
    "\n",
    "# obtain solution\n",
    "solution = model.optimize()\n",
    "\n",
    "# print optimal value of our objective function\n",
    "print(\"Optimal objective value Ex_g = %f\" % solution.objective_value)\n",
    "\n",
    "# print a summary of the model fluxes that provide optimal objective value\n",
    "solution.fluxes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows that *A* is required in the media for the cell to produce *G*.\n",
    "\n",
    "Such ideas as this can be extended to real cases. By simulating what happens to the model when simulating different growth media, researchers can design tailor-made media to maximize growth and compound production from cells."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In tomorrow's self-study session you will use some of these skills, and get introduced to some more functions, that help you to analyse larger models of metabolic systems!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
